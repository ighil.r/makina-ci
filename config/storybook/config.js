/* eslint-disable import/no-extraneous-dependencies */
import { configure, addDecorator } from '@storybook/vue'
import '../../src/plugins/vuetify'
addDecorator( () => {
  return { template: "<v-app><story/></v-app>" }
})

const req = require.context('../../src', true, /.stories.js$/)

function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
