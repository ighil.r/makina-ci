import { mount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import Home from './Home';

Vue.use(Vuetify);
Vue.use(VueRouter);

const router = new VueRouter();

test('renders correctly', () => {
  const wrapper = mount(Home, {
    router,
    mocks: {
      $t: () => {},
    },
  });
  expect(Home.element).toMatchSnapshot();
});
