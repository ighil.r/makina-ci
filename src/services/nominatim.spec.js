import nominatim from '@/services/nominatim';
import Axios from 'axios';

describe('nominatim service', () => {
  test('Has get getNominatimData function', () => {
    expect(nominatim.getNominatimData).toBeDefined();
  });


  test('Call the default URL', (done) => {
    Axios.get = jest.fn((url) => {
      expect(url).toBe('http://nominatim.openstreetmap.org/search/toto?format=json');
      return Promise.resolve({ data: 'tata' });
    });
    nominatim.getNominatimData('toto').then((data) => {
      expect(data).toBe('tata');
      done();
    });
  });
});
