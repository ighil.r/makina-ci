import overpassService from '@/services/overpass';

export const GET_OVERPASS_RESULT = 'GET_OVERPASS_RESULT';
export const GET_OVERPASS_RESULT_NS = `overpass/${GET_OVERPASS_RESULT}`;
export const FETCH_RESULT = 'FETCH_RESULT';
export const UPDATE_RESULT = 'UPDATE_RESULT';
export const ERROR_API = 'ERROR_API';

const actions = {
  [GET_OVERPASS_RESULT]({ commit }, { bbox }) {
    commit(FETCH_RESULT);
    overpassService.getOverpassData(bbox)
      .then((json) => {
        commit(UPDATE_RESULT, { results: json || [] });
      }).catch((err) => {
        commit(ERROR_API, { error: err });
      });
  },
};

export default actions;
