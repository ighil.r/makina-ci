import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import MyAddress from './Address.vue';

storiesOf('MyAddress', module)
  .add('Test address', () => ({
    components: { MyAddress },
    template: `<v-app>
                <my-address
                :address="{
                      road: '52 rue Jacques Babinet',
                      zipCode: '31000',
                      city: 'Toulouse',
                      complement: 'Makina Corpus',
                      country: 'Occitanie'
                      }"
                      label="test"
                  </my-address
                </v-app>`,
    methods: { action: action('clicked') },
  }));
