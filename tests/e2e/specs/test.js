// https://docs.cypress.io/api/introduction/api.html

describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/');
    cy.contains('button', 'Découvrir l\'application finale').click();
    cy.url('include', '/search');
  });

  it('Test i18n', () => {
    cy.visit('/');
    cy.contains('h3', 'Tutoriel Vue.js par Makina Corpus');
    cy.get('.lang-menu').children().not('.current').find('img')
      .click();
    cy.contains('h3', 'Vue.js tutorial by Makina Corpus');
  });
});
